# FOR EACH VIDEO, COUNT COMMENTS.
# MAPPER OUTPUT HAS ID, 1
# 
# STEP 2. Mimic sort-and-shuffle in MR

# open m.csv for reading as a file stream named m
# open s.csv for writing as a file stream named s

m = open( "m.csv", "r")
s = open( "s.csv", "w")
# use readlines method of the file stream object to create a list called lines


# call the list sort method on your lines to sort in place (do not reassign to another variable)


# for every line in lines (remember the colon)
lines = m.readlines()
lines.sort()

# writes out each sorted line to the s.txt file
for line in lines:
    s.write(line)

# closes both files
m.close()
s.close()


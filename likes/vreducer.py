# FOR EACH VIDEO, COUNT COMMENTS.
# SORTED DATA HAS 1 COMMENT PER LINE, ALL VIDEOIDS TOGETHER
# 
# STEP 3. REDUCE videoID and counts by summing the counts into 1 record per video

# open s.csv for reading as a file stream named s
# open r.csv for writing as a file stream named r

s = open("s.csv", "r")
r = open("r.csv", "w")
# initialize thisKey to an empty string
thisKey = ""
oldKey = None
count = 0
highest = 0
highestname = ""
# initialize thisValue to 0
thisValue = 0
# output column headers
r.write('videoID,comments\n')

# for every line in s
for line in s:
  # use the line strip and split methods and assign to a list named data
  data = line.strip().split('\t')
  if len(data) != 2:
        continue
  # assign data list to named variables id and comments
  id, comments = data


  # if  id is not equal to thisKey

  if id != thisKey:
        if thisKey:
            # output the last key value pair result
            print(thisKey + '\t' + str(count) + '\n')
            r.write(thisKey + ',' + str(count) + '\n')
            if count > highest:
                highest = count
                highestname = thisKey
            count = 0
    # start over when changing keys
        thisKey = id
        thisValue = 0

    # apply the sum function
  count = count + 1

# output the final key and value
print(thisKey + '\t' + str(count) + '\n')
r.write(thisKey + ',' + str(count) + '\n')
if count > highest:
    highest = count
    highestname = thisKey

print('Video with highest comments is:')
r.write('Video with highest comments is:')
print(highestname + '\t' + str(highest) + '\n')
r.write(highestname + ',' + str(highest) + '\n')

# close the files
s.close()
r.close()
